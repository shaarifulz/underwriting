package com.underwriting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class UnderwritingApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderwritingApplication.class, args);
	}

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
		return new RestTemplate();
	}

    @Bean
    ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
}

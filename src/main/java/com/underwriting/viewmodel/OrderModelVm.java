package com.underwriting.viewmodel;

public class OrderModelVm {

	private Long id;	

	private Long productId;

	private String productCode;

	private String productName;

	private double productPrice;

	private Long customerId;

	private String customerCode;

	private String customerName;


	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public OrderModelVm(Long id, Long productId, String productCode, String productName, double productPrice,
			Long customerId, String customerCode, String customerName) {
		this.id = id;
		this.productId = productId;
		this.productCode = productCode;
		this.productName = productName;
		this.productPrice = productPrice;
		this.customerId = customerId;
		this.customerCode = customerCode;
		this.customerName = customerName;
	}

	public OrderModelVm() {
	}
	
}

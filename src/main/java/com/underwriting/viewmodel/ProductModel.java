package com.underwriting.viewmodel;

import java.sql.Date;

public class ProductModel {

	private Long id;	
	
	private String productCode;

	private String productName;

	private double productPrice;

	private Date purchaseDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@Override
	public String toString() {
		return "ProductModel [id=" + id + ", productCode=" + productCode + ", productName=" + productName
				+ ", productPrice=" + productPrice + ", purchaseDate=" + purchaseDate + "]";
	}
}

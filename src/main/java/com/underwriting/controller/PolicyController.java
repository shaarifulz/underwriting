package com.underwriting.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.underwriting.payload.CommonViewModel;
import com.underwriting.service.OrderService;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/policy")
public class PolicyController {

	@Autowired
	OrderService orderService;
	
	@PostMapping("/save-policy")
	public ResponseEntity<Map<String, Object>> saveOrder(HttpServletRequest request, @RequestBody CommonViewModel viewModel){
		
		Map<String, Object> data = orderService.saveOrder2(viewModel);
		ResponseEntity<Map<String,Object>> responseEntity = null;
		
		responseEntity = new ResponseEntity<Map<String,Object>>(data, HttpStatus.OK);	
		return responseEntity;
	}
	
	@PostMapping("/get-policy")
	public ResponseEntity<Map<String, Object>> getOrders(HttpServletRequest request){
		
		Map<String, Object> data = orderService.getOrders();
		ResponseEntity<Map<String,Object>> responseEntity = null;
		
		responseEntity = new ResponseEntity<Map<String,Object>>(data, HttpStatus.OK);	
		return responseEntity;
	}
	
}

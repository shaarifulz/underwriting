package com.underwriting.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.underwriting.payload.CommonViewModel;
import com.underwriting.util.ICommonApiService;
import com.underwriting.util.ICommonApiServiceFeignCall;
import com.underwriting.viewmodel.OrderModelVm;
import com.underwriting.viewmodel.ProductModel;

@Service
public class OrderService{

	@Autowired
	ObjectMapper objectMapper;
	
	@Value("${get_customer_by_id}")
	String UrlGetCustomerById;
	
	//@Value("${get_product_by_id}")
	String UrlGetProductById;
	
	@Autowired
	ICommonApiService commonApiService;
	
	@Autowired
	ICommonApiServiceFeignCall feignCall;
	

	public Map<String, Object> saveOrder(CommonViewModel viewModel) {
		Map<String, Object> data = new HashMap<String, Object>();
		CommonViewModel customer = new CommonViewModel();
		ProductModel product = new ProductModel();
		OrderModelVm order = new OrderModelVm();
		//SERVICE CALL
		Map<String, Object> customerResp = commonApiService.getResponseMap(viewModel, UrlGetCustomerById);
		
		if (customerResp!=null) {
			customer = objectMapper.convertValue(customerResp.get("data"), CommonViewModel.class);
			if (customer!=null) {
				System.out.println("-=-------------------------> "+ customer.toString());
			}
		}
		
		//SERVICE CALL
		Map<String, Object> productMap = commonApiService.getResponseMap(viewModel, UrlGetProductById);
		
		if (productMap!=null) {
			product = objectMapper.convertValue(productMap.get("data"), ProductModel.class);
			if (product!=null) {
				System.out.println("-=-------------------------> "+ product.getProductName());
			}
		}
		
			OrderModelVm ord = new OrderModelVm();
			ord.setProductName(customer.getUsername());
			ord.setCustomerName(product.getProductName());
			data.put("data", ord);
		
		return data;
	}

	public Map<String, Object> saveOrder2(CommonViewModel viewModel) {
		Map<String, Object> data = new HashMap<String, Object>();
		CommonViewModel customer = new CommonViewModel();
		ProductModel product = new ProductModel();
		OrderModelVm order = new OrderModelVm();
		
		//SERVICE CALL
		ResponseEntity<CommonViewModel> feignResp = feignCall.getCustomerById2(1L);
		
		CommonViewModel resp = feignResp.getBody();
		System.out.println(resp.toString());
		
			OrderModelVm ord = new OrderModelVm();
			ord.setProductName(customer.getUsername());
			ord.setCustomerName(product.getProductName());
			data.put("data", ord);
		
		return data;
	}
	
	public Map<String, Object> getOrders() {
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<OrderModelVm> orderList = new ArrayList<>();
		OrderModelVm order1 = new OrderModelVm(1L, 2L, "productCode", "productName", 1000.0,
				3L, "customerCode", "customerName");
		OrderModelVm order2 = new OrderModelVm(1L, 2L, "productCode2", "productName2", 1000.0,
				3L, "customerCode2", "customerName2");
		
		orderList.add(order1);
		orderList.add(order2);
		
		data.put("data", orderList);
		return data;
	}

}

package com.underwriting.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.underwriting.payload.CommonViewModel;

@FeignClient(name = "customer-service", url = "http://localhost:8082", path = "/customer") 
public interface ICommonApiServiceFeignCall {

	@GetMapping("/get-customer-by-id/{id}") 
    public ResponseEntity<CommonViewModel> getCustomerById2(@PathVariable("id") Long id); 
  
}

package com.underwriting.util;

import java.util.Map;

import com.underwriting.payload.CommonViewModel;

public interface ICommonApiService {
	public Map<String, Object> getResponseMap(CommonViewModel commonViewModel, String url);
}

